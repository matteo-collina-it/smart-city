package processes;

import javafx.util.Pair;
import model.DataSensorMessage;
import model.EdgeNode;
import model.EdgeNodeMessage;
import model.utils.Utils;
import services.ServiceManager;
import services.edge.CityClient;
import simulator.Measurement;
import simulator.PM10Simulator;
import simulator.SensorStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

public class SimulatorProcess {
    private static CityClient client = new CityClient();
    private static HashMap<String,Long> mapTimer = new HashMap<>(); //idSensore - Timestamp
    private static HashMap<String,Pair<Integer,Integer>> mapSensor = new HashMap<>(); //idSensore - Coordinate sensor in city
    private static HashMap<String,EdgeNode> mapComunication = new HashMap<>();
    private static final int SECONDS_INTERVAL_SEND_STATS = 10;
    private static final long millisInOneDay = TimeUnit.DAYS.toMillis(1);

    public static void main(String[] args) throws IOException {
        BufferedReader inFromUser = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("\nQuanti sensori devo fare partire?\n");
        String totSensorStr = inFromUser.readLine();

        System.out.println("\nIndirizzo del Server Cloud (stringa vuota per usare localhost)?\n");
        String addressServerCloud = inFromUser.readLine();
        addressServerCloud = addressServerCloud.compareTo("") == 0
                ? ServiceManager.PROTOCOL + ServiceManager.BASE_URL
                : addressServerCloud;
        System.out.println(addressServerCloud + " è l'indirizzo del Server Cloud");

        int totSensor = 0;
        try {
            totSensor = Integer.parseInt(totSensorStr);
            for(int i=0; i<totSensor; i++){
                //Get nearest edge node
                PM10Simulator s = new PM10Simulator((new SensorStream() {
                    @Override
                    public void sendMeasurement(Measurement m) {
                        Socket cSocket = null;
                        long curSeconds = TimeUnit.MILLISECONDS.toSeconds(millisInOneDay - m.getTimestamp());
                        EdgeNode edgeNode = new EdgeNode();

                        //Passati 10s
                        if(!mapTimer.containsKey(m.getId()) ||
                                (mapTimer.get(m.getId()) - curSeconds) > SECONDS_INTERVAL_SEND_STATS) {

                            //Tramite le coordinate del sensore, prendo il nodo + vicino
                            Pair<Integer, Integer> coordinate = mapSensor.get(m.getId());
                            edgeNode = client.getNearestNode(coordinate.getKey(), coordinate.getValue());
                            mapComunication.put(m.getId(),edgeNode); //setta il nodo col quale comunicherà il sensore
                            try{
                                System.out.print("\n" + "Il sensore " + m.getId() + " comunica con il nodo edge " + edgeNode.getId() + "\n");
                            }catch (Exception ex){
                                //error on print
                            }
                            mapTimer.put(m.getId(), curSeconds);
                        }

                        edgeNode = mapComunication.containsKey(m.getId()) ? mapComunication.get(m.getId()) : new EdgeNode(); //prendo il nodo col quale comunica il sensore

                        if(edgeNode.getId() != 0) {
                            try {
                                cSocket = new Socket(edgeNode.getIp(), edgeNode.getPortListenFromSensor());
                            } catch (IOException e) {
                                //Se non riesce a mandare i dati al nodo, al prossimo step chiede il nodo + vicino
                                System.err.print("\nIl sensore " + m.getId() + " non riesce a stabilire una connessione con il nodo edge " + edgeNode.getId() + "\n");
                            }
                            try {
                                DataOutputStream outTo = new DataOutputStream(cSocket.getOutputStream());
                                DataSensorMessage msg = new DataSensorMessage(m.getId(), m.getValue(), m.getTimestamp());
                                //System.out.print("\nWriting: "+msg.toString());
                                outTo.writeBytes(msg.toGsonString() + "\n");
                                cSocket.close();
                            } catch (Exception e) {
                                //Se non riesce a mandare i dati al nodo, al prossimo step chiede il nodo + vicino
                                System.err.print("\nIl sensore " + m.getId() + " non riesce a stabilire una connessione con il nodo edge " + edgeNode.getId() + "\n");
                            }
                        }
                    }
                }));
                //Generazione coordinate per simulatore
                Pair<Integer,Integer> coordinate = Utils.getRandomCoordinateCity();
                mapSensor.put(s.getIdentifier(),coordinate);
                s.start();
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }
}
