package processes;

import model.EdgeNode;
import model.GlobalStat;
import model.StandardDeviation;
import model.utils.Utils;
import services.edge.CityClient;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

/* Applicazione CLIENT ANALISTA */
public class AnalystProcess {
    private static CityClient client = new CityClient();

    public static void main(String[] args){
        System.out.println("[[ Inizializzazione del processo di analisi dati ]]\n");
        String info = "\n\nPremi [1] (e INVIO) per richiedere lo stato attuale della città (posizione dei vari nodi edge nella griglia)" +
                "\nPremi [2 \t Numero stats richieste \t Id nodo edge] (e INVIO) per richiedere le ultime n statistiche del nodo edge indicato" +
                "\nPremi [3 \t Numero stats richieste] (e INVIO) per richiedere le ultime n statistiche della città" +
                "\nPremi [4 \t Numero stats richieste \t Id nodo edge] (e INVIO) per richiedere la deviazione standard e media delle ultime n statistiche di un nodo edge" +
                "\nPremi [5 \t Numero stats richieste] (e INVIO) per richiedere la deviazione standard e media delle ultime n statistiche della città" +
                "\nPremi 0 (e INVIO) per uscire\n";

        new Thread(() -> {
            String method = "";

            while (!method.equals("0")) {
                BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
                System.out.print(info);

                String input = null;
                try {
                    input = reader.readLine();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                String[] commands = input.split(" ");
                method = commands[0];
                System.out.println("\nMETHOD: " + method);
                //System.out.println("\nMETHOD: " + method + "\t(" + Utils.getCurrentHourMinSec() + ")");

                switch (method){
                    //Stato della città
                    case "1":{
                        System.out.print("\nStato della città:");
                        ArrayList<EdgeNode> list = new ArrayList<>(client.getStatusCity());
                        String status = "[ ";
                        for (int i=0;i<list.size();i++){

                            status += ("Nodo " + list.get(i).getId() + " (" +
                                    Integer.toString(list.get(i).getCoordinate().getKey()) +
                                    "," +
                                    Integer.toString(list.get(i).getCoordinate().getValue()) +
                                    ")"
                            );
                            status += (i==list.size()-1 ? "" : ",");
                        }
                        status += " ]";
                        System.out.print("\n" + status + "\n");
                        break;
                    }
                    //Ultime n statistiche di un nodo edge
                    case "2":{
                        int n = 0;
                        try {
                            n = Integer.parseInt(commands[1]);
                        }catch (Exception ex){
                            System.err.print("\nIl numero di statistiche richiesto non è valido");
                        }

                        int edgeId = 0;
                        try {
                            edgeId = Integer.parseInt(commands[2]);
                        }catch (Exception ex){
                            System.err.print("\nIl valore del nodo edge indicato non è valido");
                        }

                        System.out.print("\nUltime "+n+" statistiche del nodo edge ("+edgeId+") :");
                        List<GlobalStat> list = client.getLastNStatsOfEdgeId(n,edgeId);
                        if(list != null){
                            printLastStats(list,false);
                        }else{
                            System.out.print("\nIl nodo indicato non ha alcuna statistica registrata\n");
                        }
                        break;
                    }
                    //Ultime n statistiche della città
                    case "3":{
                        int n = 0;
                        try {
                            n = Integer.parseInt(commands[1]);
                        }catch (Exception ex){
                            System.err.print("\nIl numero di statistiche richiesto non è valido");
                        }

                        System.out.print("\nUltime "+n+" statistiche della città :");
                        List<GlobalStat> list = client.getLastNStatsOfCity(n);
                        if(list != null){
                            printLastStats(list,true);
                        }else{
                            System.out.print("\nIl nodo indicato non ha alcuna statistica registrata\n");
                        }
                        break;
                    }
                    default:{
                        System.out.print("\nIl comando inserito non è valido");
                        break;
                    }
                    //Deviazione standard e media delle ultime n statistiche di un nodo edge
                    case "4":{
                        int n = 0;
                        try {
                            n = Integer.parseInt(commands[1]);
                        }catch (Exception ex){
                            System.err.print("\nIl numero di statistiche richiesto non è valido");
                        }

                        int edgeId = 0;
                        try {
                            edgeId = Integer.parseInt(commands[2]);
                        }catch (Exception ex){
                            System.err.print("\nIl valore del nodo edge indicato non è valido");
                        }

                        System.out.print("\nDeviazione standard e media delle ultime "+n+" statistiche del nodo edge ("+edgeId+") :");
                        StandardDeviation std = client.getDeviationOfNStatsOfEdgeId(n,edgeId);
                        if(std != null){
                            System.out.print("\nDeviazione standard : " + std.standardDeviation + " - Avg : " + std.avg);
                        }else{
                            System.out.print("\nIl nodo indicato non ha alcuna statistica registrata\n");
                        }
                        break;
                    }
                    //Deviazione standard e media delle ultime n statistiche della città
                    case "5":{
                        int n = 0;
                        try {
                            n = Integer.parseInt(commands[1]);
                        }catch (Exception ex){
                            System.err.print("\nIl numero di statistiche richiesto non è valido");
                        }

                        System.out.print("\nDeviazione standard e media delle ultime "+n+" statistiche della città :");
                        StandardDeviation std = client.getDeviationOfNStatsOfCity(n);
                        if(std != null){
                            System.out.print("\nDeviazione standard : " + std.standardDeviation + " - Avg : " + std.avg);
                        }else{
                            System.out.print("\nLa città non ha alcuna statistica registrata\n");
                        }
                        break;
                    }
                }
            }

        }).start();
    }

    private static void printLastStats(List<GlobalStat> list,boolean globalStats){
        String msg = "[ ";
        for (int i=0;i<list.size();i++){

            String nodoId = globalStats ? ( (list.get(i).edgeId != 0 ? Integer.toString(list.get(i).edgeId) : "G") + " - ") : "";
            msg += nodoId + " (v: " + Double.toString(list.get(i).getValue())
                    + " , t: " +
                    Long.toString(list.get(i).getTimestamp()) + ")";

            msg += (i==list.size()-1 ? "" : ",");
        }
        msg += " ]";
        System.out.print("\n" + msg + "\n");
    }
}
