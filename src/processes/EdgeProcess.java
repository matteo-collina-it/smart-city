package processes;

import com.google.gson.Gson;
import jdk.nashorn.internal.objects.Global;
import model.*;
import model.utils.Utils;
import services.ServiceManager;
import services.edge.CityClient;
import structure.BufferLocalStats;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Array;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.*;

public class EdgeProcess {
    private static CityClient client = new CityClient();
    private static EdgeNode edgeNode = new EdgeNode(Utils.getRandomInt(0,1000),
            ServiceManager.BASE_URL,
            ServiceManager.getSocket(),
            ServiceManager.getSocket(),
            Utils.getRandomPointInCity(),
            Utils.getRandomPointInCity());
    private static HashMap<Integer,EdgeNode> viewEdgeNodes; //i nodi che conosce il nodo corrente (coordinatore + quelli con id > del suo)
    private static boolean isClosing = false;
    private static boolean isOnElectionMode = false;
    private static boolean isElectionStartsFromOtherEdges = false;
    private static List<EdgeNode> listNodes;
    static ServerSocket serversocket;
    private static Socket connectionSocketEdge;
    private static Socket connectionSocketCoordinator = new Socket();
    private static DataOutputStream outToCoordinator;
    private static BufferedReader inFromCoordinator;
    static ServerSocket serversocketSensor;
    private static Thread threadComunicate;
    private static ArrayList<DataStats> bufferGlobal; //globale, esclusivo per il coordinatore (array di statistiche)
    private static BufferLocalStats<DataSensorMessage> buffer;  //locale per ogni nodo (array di messaggi)
    private static BufferedReader inFromEdge;
    private static DataOutputStream outToEdge;
    private static CounterMsgElection msgReceivedOkElection = new CounterMsgElection();


    private static final int SIZE_CALCULATE_AVG = 40;
    private static final int SECONDS_TO_SEND_STATS_TO_CLOUD = 5;
    private static EdgeNode coordinator = new EdgeNode(); //puntatore al coordinatore
    private static final int SECONDS_TIMEOUT_ELECTION = 5;

    public static void main(String[] args) {
        buffer = new BufferLocalStats<>(SIZE_CALCULATE_AVG);
        bufferGlobal = new ArrayList<>();
        viewEdgeNodes = new HashMap<>();
        listNodes = insertNode();

        if (listNodes.size() > 0){
            Thread threadConnection = new Thread(() -> {
                while(true){
                    System.out.println("\n\n[[ HIT RETURN TO STOP ... ]]");
                    char cmd = '-';
                    try {
                        cmd = (char) System.in.read();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    if(cmd == '\n'){
                        break;
                    }
                }
                //Mantengo il controllo del nodo che deve uscire dalla rete
                synchronized (edgeNode){
                    EdgeProcess.isClosing = true;
                    if(EdgeProcess.closeConnection()){
                        System.out.println("\n----- FINE Chiusura connessione " + edgeNode.getId() + " -----\n");
                    }
                }
            });

            //Caso: solo il nodo stesso nella città
            if(listNodes.size()==1){
                setMeAsCoordinator();
            }

            //Start threads
            threadConnection.start();
            setComunicationChannel(listNodes);

        }else{
            //Nel caso in cui non sia riuscito ad entrare
            return;
        }
    }


    private static List<EdgeNode> insertNode(){
        return client.addNode(edgeNode);
    }
    private static boolean closeConnection(){
        System.out.print("\n\n----- INIZIO processo di uscita dal network del nodo ("+edgeNode.getId()+") -----\n");
        try{
            serversocket.close();
            System.out.print("\nLa Server Socket del nodo ("+edgeNode.getId()+") in ascolto dai nodi edge è chiusa √");
        }catch (Exception exception){
            System.err.print("\nErrore nella chiusura della porta in ascolto dai nodi edge del nodo: "+ edgeNode.getId());
        }

        try{
            serversocketSensor.close();
            System.out.print("\nLa Server Socket del nodo ("+edgeNode.getId()+") in ascolto dai sensori è chiusa √");
        }catch (Exception exception){
            System.err.print("\nErrore nella chiusura della porta in ascolto dai sensori del nodo: "+ edgeNode.getId());
        }

        return client.removeNode(edgeNode);
    }
    private static void setMeAsCoordinator(){
        System.out.print("\n[C] Nuovo cordinatore:  " + edgeNode.getId() + "\n");
        synchronized (edgeNode){
            edgeNode.setiAmCoordinator();
        }
        coordinator = edgeNode;

        //Richiamato Ogni $SECONDS_TO_SEND_STATS_TO_CLOUD secondi
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                double avgGlobal = 0;
                ArrayList<DataStats> tmpBufferGlobal ;
                synchronized (bufferGlobal){
                    tmpBufferGlobal = new ArrayList<>(bufferGlobal);
                }

                //- Calcolo avg totale
                //- Mando avg totale al server
                if(tmpBufferGlobal.size() > 0) {
                    for (int i = 0; i < tmpBufferGlobal.size(); i++) {
                        avgGlobal += tmpBufferGlobal.get(i).getStat();
                    }
                    avgGlobal /= tmpBufferGlobal.size();

                    if(!isOnElectionMode && !isElectionStartsFromOtherEdges)
                        System.out.print("\n"+Utils.getCurrentHourMinSec()+" [+] AVG_GLOBALE calcolata (ogni 5s) da "+tmpBufferGlobal.size()+" locale/i : " + avgGlobal + "\n");

                    GlobalDataStats g = new GlobalDataStats(tmpBufferGlobal, avgGlobal, Utils.getCurrentTimestamp());
                    client.sendGlobalStat(g);
                }else{
                    System.out.print("\n"+Utils.getCurrentHourMinSec()+" [+] AVG_GLOBALE calcolata (ogni 5s) non inviata, il buffer globale è vuoto\n");
                }

                synchronized (bufferGlobal){
                    for (int i = 0; i < tmpBufferGlobal.size(); i++) {
                        bufferGlobal.remove(0);
                    }
                }
            }
        }, 0, SECONDS_TO_SEND_STATS_TO_CLOUD*1000);
    }
    private static void setCoordinator(EdgeNode edgeNode){
        try{
            viewEdgeNodes.remove(coordinator.getId());
            //System.out.print("\nIl vecchio coordinatore è stato cancellato dalla view");
        }catch (Exception ex){
            System.err.print("\nIl vecchio coordinatore non esisteva, pertanto non è stato cancellato  dalla view");
        }
        coordinator = edgeNode;
        System.out.print("\n"+Utils.getCurrentHourMinSec()+"\t[C] Salvato il coordinatore: "+coordinator.getId());
    }
    /*
    * Fa partire l'elezione
    * */
    private static void startElection(){
        System.out.print("\n\n------ START ELECTION ------\n\n");
        isOnElectionMode = true;
        //System.out.print("\nSize della edgeView del nodo corrent: " + viewEdgeNodes.size());

        /*
        * Sincronizzato sulla view perchè se entra un nodo durante l'elezione, non può inserirsi
        * - rimuovo il coordinatore
        * - verifico se la view ha dimensione 0 => sotto il nodo corrente come coordinatore
        * - deve dirlo a tutti gli altri nodi
        * */
        synchronized (viewEdgeNodes){
            viewEdgeNodes.remove(coordinator.getId());

            //System.out.print("\nRimuovo il coordinatore...");
            //System.out.print("\nSize della edgeView del nodo corrent: " + viewEdgeNodes.values().size());


            new Thread(()->{
                try{
                    //System.out.print("\n" + Utils.getCurrentHourMinSec() + " Start timeout election");
                    Thread.sleep(SECONDS_TIMEOUT_ELECTION*1000);
                    if(!isElectionStartsFromOtherEdges)
                        onFinishTimeoutElection();
                    else
                        System.out.print("\nElezione non conclusa poichè è già partita da un nodo con ID maggiore");
                }catch (Exception ex){
                    ex.printStackTrace();
                }
            }).start();


            new Thread(()->{
                int count = 0;

                for (EdgeNode e : viewEdgeNodes.values()){
                    try{
                        Socket clientSocket = new Socket(e.getIp(), e.getPortListenFromEdges());
                        DataOutputStream outTo = new DataOutputStream(clientSocket.getOutputStream());

                        //Send message of election
                        EdgeNodeMessage electionEdgeNodeMessage = new EdgeNodeMessage(EdgeNodeMessage.MessageType.ELECTION,edgeNode);
                        outTo.writeBytes(electionEdgeNodeMessage.toGsonString()+"\n");
                        System.out.print("\n"+Utils.getCurrentHourMinSec()+"\tSending msg : "+electionEdgeNodeMessage.toString() + "TO: " + e.getId());
                    }catch (IOException exception){
                        System.err.print("\nError to comunicate in election with edge node " + e.getId()+
                                "\n" + exception.toString() + "\n");
                        exception.printStackTrace();
                    }
                }
            }).start();
        }
    }

    /*
    * Metodo chiamato quando finisce il timeout dell'elezione
    * */
    private static void onFinishTimeoutElection(){
        System.out.print("\n" + Utils.getCurrentHourMinSec() + " Timeout is end");
        int sizeViewEdgeNodes = 0;
        synchronized (viewEdgeNodes) {
            sizeViewEdgeNodes = viewEdgeNodes.size();
        }

        //System.out.print("\nsizeViewEdgeNodes: " + sizeViewEdgeNodes);
        if ((sizeViewEdgeNodes == 0  || msgReceivedOkElection.size()==0) && !isElectionStartsFromOtherEdges && isOnElectionMode) {
            if(msgReceivedOkElection.size()==0)
                System.out.print("\n"+Utils.getCurrentHourMinSec()+"\tSettato come coordinatore perchè non ha ricevuto ok msg");

            setMeAsCoordinator();

            //Prende tutti i nodi inferiori e glielo comunico
            new Thread(() -> {
                ArrayList<EdgeNode>listAllNodes = new ArrayList<>(client.getListNodes());
                synchronized (listAllNodes){
                    for (int i=0;i<listAllNodes.size();i++){
                        if (listAllNodes.get(i).getId() == edgeNode.getId()){
                            listAllNodes.remove(i);
                            break;
                        }
                    }
                }

                //for (EdgeNode e : tmpViewTmpEdgeNodesForElection.values()) {
                for (EdgeNode e : listAllNodes) {
                    try {
                        Socket clientSocket = new Socket(e.getIp(), e.getPortListenFromEdges());
                        DataOutputStream outTo = new DataOutputStream(clientSocket.getOutputStream());

                        //Send message of election
                        EdgeNodeMessage electionEdgeNodeMessage = new EdgeNodeMessage(EdgeNodeMessage.MessageType.COORDINATOR, edgeNode);
                        outTo.writeBytes(electionEdgeNodeMessage.toGsonString() + "\n");
                        System.out.print("\nSending msg : " + electionEdgeNodeMessage.toString() + "TO: " + e.getId());
                    } catch (IOException exception) {
                        System.err.print("\nError to comunicate in election with edge node " + e.getId() +
                                "\n" + exception.toString() + "\n");
                        exception.printStackTrace();
                    }
                }
                onFinishElection();


            }).start();
        }
    }

    /*
    * Metodo richiamato quando finisce l'elezione del cordinatore
    * */
    private static void onFinishElection(){
        msgReceivedOkElection.reset();
        isOnElectionMode = false;
        isElectionStartsFromOtherEdges = false;
        if (edgeNode.amICoordinator())
            System.out.print("\n\n------ END ELECTION ------\n\n");
    }

    /*
    * Set comunication channel between current node and others in city
    * - Il processo si mette in ascolto su Ip e porta che ha come attributi
    * - Apro una socket client
    * */
    private static void setComunicationChannel(List<EdgeNode> list){

        //In ascolto dagli altri edge
        Thread threadListenEdge = new Thread(()-> {
            try {
                serversocket = new ServerSocket(edgeNode.getPortListenFromEdges());
                while (true) {
                    connectionSocketEdge = serversocket.accept();

                    try{
                        inFromEdge = new BufferedReader(
                                new InputStreamReader(connectionSocketEdge.getInputStream()));;
                        outToEdge = new DataOutputStream(connectionSocketEdge.getOutputStream());
                    }
                    catch (IOException e) {
                        e.printStackTrace();
                    }


                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                EdgeNodeMessage edgeNodeMessage = new Gson().fromJson(inFromEdge.readLine(), EdgeNodeMessage.class);
                                //System.out.print("\n\nHo ricevuto : "+edgeNodeMessage.toString());

                                switch (edgeNodeMessage.getMessageType()){
                                    /*
                                    * Quando ricevo un nodo di presentazione di un nodo, rispondo OK
                                    * Se ha id > del corrente, lo salvo
                                    * */
                                    case NEW_NODE:{
                                        if(edgeNodeMessage.getEdgeNode().getId() > edgeNode.getId()){
                                            synchronized (viewEdgeNodes){
                                                viewEdgeNodes.put(edgeNodeMessage.getEdgeNode().getId(),edgeNodeMessage.getEdgeNode());
                                            }
                                            System.out.print("\nAggiunto nodo " + edgeNodeMessage.getEdgeNode().getId() +
                                                    " alla view locale del processo perchè ha id > del nodo del processo corrente");
                                        }

                                        EdgeNodeMessage responseMessage = new EdgeNodeMessage(EdgeNodeMessage.MessageType.OK,edgeNode);
                                        outToEdge.writeBytes(responseMessage.toGsonString()+"\n");
                                        break;
                                    }
                                    /*
                                     * Quando ricevo una statistica da un altro nodo edge (presuppone che io sia il coordinatore)
                                     * La salvo nel buffer
                                     * Risponde con la statistica globale più recente
                                     * */
                                    case STATS:{
                                        int sizeGlobalBuffer = 0;
                                        synchronized (bufferGlobal){
                                            bufferGlobal.add(edgeNodeMessage.getStats());
                                            sizeGlobalBuffer = bufferGlobal.size();
                                        }

                                        //Stampo solo se non è lo stesso nodo
                                        if(edgeNodeMessage.getEdgeNode().getId() != edgeNode.getId() && !isOnElectionMode)
                                            System.out.print("\n"+Utils.getCurrentHourMinSec()+" [+] AVG ricevuta da nodo edge ("+edgeNodeMessage.getEdgeNode().getId()+") : " + edgeNodeMessage.getStats().getStat() +   "\n");


                                        GlobalStat globalStat = client.getLatestGlobalStat();
                                        EdgeNodeMessage responseMessage =
                                                new EdgeNodeMessage(EdgeNodeMessage.MessageType.GLOBAL_STATS,edgeNode,globalStat);
                                        //System.out.print("\n\nSending msg : "+responseMessage.toString());
                                        outToEdge.writeBytes(responseMessage.toGsonString()+"\n");
                                        break;
                                    }

                                    /*
                                    * Ricevo chi mi sta mandando un messaggio di elezione (ccoloro con id < del mio)
                                    * Setto un timeout (*il metodo richiamato quando finisce il timeout è onFinishTimeoutElection),
                                    * dopo il quale
                                    * verifico se nella viewEdge è vuota (vuol dire che sono quello con id maggiore), allora
                                    * comunico a tutti i nodi inferiori che io sono il nuovo coordinatore
                                    * */
                                    case ELECTION:{
                                        EdgeNodeMessage.MessageType type = EdgeNodeMessage.MessageType.OK_ELECTION;
                                        if(isOnElectionMode && edgeNodeMessage.getEdgeNode().getId() < edgeNode.getId()){
                                            System.out.print("\nUn'altra elezione era partita dal nodo " +
                                                    edgeNodeMessage.getEdgeNode().getId() + " ma è stata bloccata perchè il nodo corrente ("+edgeNode.getId()+") è più grande");
                                            type = EdgeNodeMessage.MessageType.STOP_ELECTION;
                                        }

                                        System.out.print("\n"+Utils.getCurrentHourMinSec()+"\t[ELECTION] nodo ricevuto ("+edgeNodeMessage.getEdgeNode().getId()+")");
                                        EdgeNodeMessage responseElectionMessage = new EdgeNodeMessage(type,edgeNode);
                                        try{
                                            Socket clientSocket = new Socket(edgeNodeMessage.getEdgeNode().getIp(),
                                                    edgeNodeMessage.getEdgeNode().getPortListenFromEdges());
                                            DataOutputStream outTo = new DataOutputStream(clientSocket.getOutputStream());

                                            System.out.print("\n"+Utils.getCurrentHourMinSec()+"\t[ELECTION] inviando msg a ("+edgeNodeMessage.getEdgeNode().getId()+") of type " + type
                                            );
                                            outTo.writeBytes(responseElectionMessage.toGsonString()+"\n");

                                        }catch (Exception ex){
                                            System.err.print("\nError to comunicate ack OK ELECTION");
                                        }
                                        break;
                                    }
                                    /*
                                    * Se un nodo con id < del corrente ha indetto un elezione che era già iniziata
                                    * dal corrente che ha id >, allora riferisco di stoppare
                                    * */
                                    case OK_ELECTION:{
                                        msgReceivedOkElection.addOne();
                                        System.out.print("\n"+Utils.getCurrentHourMinSec()+"\tOK_ELECTION from " + edgeNodeMessage.getEdgeNode().getId() + " (to received "+msgReceivedOkElection.size()+")");
                                        break;
                                    }
                                    case STOP_ELECTION:{
                                        isElectionStartsFromOtherEdges = true;
                                        System.out.print("\n"+Utils.getCurrentHourMinSec()+"\tSTOP_ELECTION from " + edgeNodeMessage.getEdgeNode().getId());
                                        break;
                                    }
                                    /*
                                     * Ricevo il coordinatore
                                     * */
                                    case COORDINATOR:{
                                        synchronized (coordinator) {
                                            setCoordinator(edgeNodeMessage.getEdgeNode());
                                        }
                                        onFinishElection();
                                        break;
                                    }
                                    default:{
                                        EdgeNodeMessage responseMessage = new EdgeNodeMessage(EdgeNodeMessage.MessageType.OK,edgeNode);
                                        outToEdge.writeBytes(responseMessage.toGsonString()+"\n");
                                        break;
                                    }
                                }
                            }
                            catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }).start();
                }
            } catch (IOException ex) {
                System.err.print("\nLa socket server in ascolto per i nodi edge non è più raggiungibile "+
                        "\nIP: " + edgeNode.getHttpIp() +
                        "\nPORT: " + edgeNode.getPortListenFromEdges());
            }
        });


        //In ascolto dai sensori
        Thread threadListenSensor = new Thread(()-> {
            try {
                serversocketSensor = new ServerSocket(edgeNode.getPortListenFromSensor());

                while (true) {
                    Socket connectionSensorSocket = serversocketSensor.accept();
                    BufferedReader inFromSensor = new BufferedReader(
                            new InputStreamReader(connectionSensorSocket.getInputStream()));

                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                DataSensorMessage dataSensorMessage = new Gson().fromJson(inFromSensor.readLine(), DataSensorMessage.class);
                                //Riceve i dati dai sensori
                                int sizeBuffer = 0;
                                //L'add delle stats al buffer locale è sincronizzato
                                //poichè più sensori possono aggiungere stats al buffer locale
                                synchronized (buffer) {
                                    try {
                                        buffer.put(dataSensorMessage);
                                        sizeBuffer = buffer.size();
                                    } catch (InterruptedException e) {
                                        e.printStackTrace();
                                    }
                                }

                                //Se il buffer arriva a 40, calcolo la media dei 40 e tolgo i primi 20
                                if(sizeBuffer >= SIZE_CALCULATE_AVG){
                                    calculateAvgAndSendStatsToCoordinator();
                                }

                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }).start();
                }

            } catch (IOException ex) {
                //Se il nodo non è più attivo e non può ascoltare i sensori, i dati vengono persi
                System.err.print("\nLa socket server in ascolto per i sensori non è più raggiungibile "+
                        "\nIP: " + edgeNode.getHttpIp() +
                        "\nPORT: " + edgeNode.getPortListenFromSensor());
            }
        });




        //In comunicazione esterna
        threadComunicate = new Thread(new Runnable() {
            @Override
            public void run() {
                List<EdgeNode> listEdgeNodes = new ArrayList<>(list);
                for (EdgeNode e : listEdgeNodes){
                    if(edgeNode.getId() != e.getId()){
                        try{
                            Socket clientSocket = new Socket(e.getIp(), e.getPortListenFromEdges());

                            DataOutputStream outToOtherEdges = new DataOutputStream(clientSocket.getOutputStream());
                            BufferedReader inFromServerOfOTherEdges = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));


                            //Send message of presentation
                            EdgeNodeMessage presentationEdgeNodeMessage = new EdgeNodeMessage(EdgeNodeMessage.MessageType.NEW_NODE,edgeNode);
                            outToOtherEdges.writeBytes(presentationEdgeNodeMessage.toGsonString()+"\n");
                            //System.out.print("\nSending msg : "+presentationEdgeNodeMessage.toString());

                            //Wait Ack "ok"
                            EdgeNodeMessage edgeNodeMessage = new EdgeNodeMessage();
                            while (edgeNodeMessage.getMessageType() != EdgeNodeMessage.MessageType.OK) {
                                //System.out.print("\n\n... Waiting for response ok from the city ...");
                                edgeNodeMessage= new Gson().fromJson(inFromServerOfOTherEdges.readLine(), EdgeNodeMessage.class); //bloccante

                                synchronized (viewEdgeNodes){
                                    boolean hasIdGreaterThanCurrent = edgeNodeMessage.getEdgeNode().getId() > edgeNode.getId();
                                    boolean isCoordinator = edgeNodeMessage.getEdgeNode().amICoordinator();
                                    if(hasIdGreaterThanCurrent || isCoordinator){
                                        viewEdgeNodes.put(edgeNodeMessage.getEdgeNode().getId(),
                                                edgeNodeMessage.getEdgeNode());

                                        //Se gli risponde il coordinatore, se lo segna
                                        synchronized (coordinator) {
                                            if (isCoordinator) {
                                                setCoordinator(edgeNodeMessage.getEdgeNode());
                                                connectionSocketCoordinator = clientSocket;
                                                outToCoordinator = outToOtherEdges;
                                                inFromCoordinator = inFromServerOfOTherEdges;
                                            }
                                        }


                                        String msg = "\nAggiunto nodo " + edgeNodeMessage.getEdgeNode().getId() +
                                                " alla view locale del processo";
                                        if(isCoordinator){
                                            msg += " perchè è il coordinatore";
                                        }else{
                                            msg += " perchè ha id > del nodo del processo corrente";
                                        }
                                        //System.out.print(msg);
                                    }else{
                                        /*System.out.print("\nIl nodo " + edgeNodeMessage.getEdgeNode().getId() +
                                                " NON è stato aggiunto alla view locale del processo " +
                                                "perchè aveva id < del nodo corrente e non era coordinatore");*/
                                    }
                                }
                            }
                            //System.out.print("\nComunication is done √");
                        }catch (IOException exception){
                            System.err.print("\nError to comunicate with edge node " + e.getId()+
                                    "\n" + exception.toString() + "\n");
                            exception.printStackTrace();
                        }
                    }
                }
            }
        });


        threadListenEdge.start();
        threadListenSensor.start();
        threadComunicate.start();
    }

    /*
    * 1. Calcola media dei primi $TOT elementi
    * 2. Manda il valore al coordinatore, il quale risponde con la media globale corrente
    * 3. Rimuove i primi $TOT/2 elementi (per la sliding window del 50%)
    * */
    private synchronized static void calculateAvgAndSendStatsToCoordinator(){
        EdgeNodeMessage edgeNodeStatsMessage;

        //synchronized (buffer) {
            double avg = 0;
            BufferLocalStats<DataSensorMessage> bufferClone = new BufferLocalStats(buffer);
            //Avg
            for (int i = 0; i < SIZE_CALCULATE_AVG; i++) {
                try {
                    avg += bufferClone.take().getValue();
                    //Sliding Windows
                    if (i < SIZE_CALCULATE_AVG / 2) {
                        buffer.take();
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            avg /= SIZE_CALCULATE_AVG;

            DataStats dataStats = new DataStats(avg, Utils.getCurrentTimestamp(), edgeNode.getId());
            edgeNodeStatsMessage = new EdgeNodeMessage(EdgeNodeMessage.MessageType.STATS, edgeNode, dataStats);
        //}

        if (!edgeNode.amICoordinator() && !isOnElectionMode && !isElectionStartsFromOtherEdges)
            System.out.print("\n" + Utils.getCurrentHourMinSec() + " [+] AVG locale :" + Double.toString(edgeNodeStatsMessage.getStats().value) + "\n");


        try{
            int timeout = 5000; //5s
            Socket clientSocket = new Socket();
            clientSocket.connect(new InetSocketAddress(coordinator.getIp(), coordinator.getPortListenFromEdges()),timeout);

            DataOutputStream outToCoordinator = new DataOutputStream(clientSocket.getOutputStream());
            BufferedReader inFromCoordinator = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));

            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        outToCoordinator.writeBytes(edgeNodeStatsMessage.toGsonString()+"\n");
                        EdgeNodeMessage edgeNodeMessage = new EdgeNodeMessage();
                        try{
                            edgeNodeMessage = new Gson().fromJson(inFromCoordinator.readLine(), EdgeNodeMessage.class);
                        }catch (Exception ex){
                            System.err.print("\n"+ex.toString());
                        }

                        try {
                            if (!edgeNode.amICoordinator() && !isOnElectionMode && !isElectionStartsFromOtherEdges)
                                System.out.print("\n"+Utils.getCurrentHourMinSec()+" [+] AVG_GLOBALE ricevuta dal coordinatore " + edgeNodeMessage.getGlobalStats().getValue() + "\n");
                        }
                        catch (Exception ex){
                            //Avg non stampata perchè in corso elezione
                        }

                        //System.out.print("\n\n... <END Calculate AVG> ...\n\n");
                    } catch (IOException e) {
                        System.out.print("\n");
                        //e.printStackTrace();

                        if(!isOnElectionMode && !isElectionStartsFromOtherEdges) {
                            System.err.print("\nERR_ELECT_1) Le statistiche non sono state inviate poichè " +
                                    "il coordinatore (" + coordinator.getId() + ") non è disponibile \n\n");
                            startElection();
                        }
                    }
                }
            }).start();

        }catch (IOException ex){
            System.out.print("\n");
            //ex.printStackTrace();

            if(!isOnElectionMode && !isElectionStartsFromOtherEdges) {
                System.err.print("\nERR_ELECT_2) Le statistiche non sono state inviate poichè " +
                        "il coordinatore (" + coordinator.getId() + ") non è disponibile \n\n");
                startElection();
            }
        }

    }

}
