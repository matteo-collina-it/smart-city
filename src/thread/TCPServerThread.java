package thread;

import com.google.gson.Gson;
import model.EdgeNode;
import model.EdgeNodeMessage;

import java.io.*;
import java.net.*;

public class TCPServerThread extends Thread {

    private Socket connectionSocket = null;
    private EdgeNode edgeNode = null;
    private BufferedReader inFromClient;
    private DataOutputStream outToClient;

    public TCPServerThread(EdgeNode edgeNode, Socket s) {
        this.connectionSocket = s;
        this.edgeNode = edgeNode;

        try{
            inFromClient = new BufferedReader(
                            new InputStreamReader(connectionSocket.getInputStream()));
            outToClient = new DataOutputStream(connectionSocket.getOutputStream());
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void run() {
        try {
            EdgeNodeMessage edgeNodeMessage = new Gson().fromJson(inFromClient.readLine(), EdgeNodeMessage.class);
            System.out.print("\nI received: "+edgeNodeMessage.toString());

            EdgeNodeMessage responseMessage = new EdgeNodeMessage(EdgeNodeMessage.MessageType.OK,this.edgeNode);
            System.out.print("\n\nSending msg : "+responseMessage.toString());
            outToClient.writeBytes(responseMessage.toGsonString()+"\n");
            connectionSocket.close();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }
}