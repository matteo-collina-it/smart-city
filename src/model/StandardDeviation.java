package model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="StandardDeviation")
@XmlAccessorType(XmlAccessType.FIELD)
public class StandardDeviation {
    public double standardDeviation;
    public double avg;

    public StandardDeviation(){}
    public StandardDeviation(double standardDeviation, double avg){
        this.standardDeviation = standardDeviation;
        this.avg = avg;
    }
}
