package model.singleton;
import javafx.util.Pair;
import model.DataStats;
import model.EdgeNode;
import model.GlobalDataStats;
import model.GlobalStat;
import model.utils.Utils;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/*
* KEY:
* < ID, <X,Y> > = Edge
*
* */

@XmlRootElement
@XmlAccessorType (XmlAccessType.FIELD)
public class EdgeNodeSingleton {
    private static final int MIN_DISTANCE_EDGES = 20;

    private HashMap<Pair<Integer,Integer>,EdgeNode> edgeNodesMap;
    private ArrayList<GlobalStat> globalStats;
    private static EdgeNodeSingleton instance;

    public EdgeNodeSingleton() {
        edgeNodesMap = new HashMap<>();
        globalStats = new ArrayList<>();
    }

    //singleton
    public synchronized static EdgeNodeSingleton getInstance(){
        if(instance==null)
            instance = new EdgeNodeSingleton();
        return instance;
    }

    public synchronized int getCountEdgeNodes(){
        return getInstance().getEdgeNodesList().size();
    }
    public synchronized List<EdgeNode> getEdgeNodesList() {
        return new ArrayList<>(edgeNodesMap.values());
    }

    public synchronized void add(EdgeNode e){
        try {
            edgeNodesMap.put(e.getCoordinate(),e);
        } catch(Exception exc){
            System.err.print(exc.toString());
        }
    }

    public synchronized EdgeNode getNodeWithId(int id){
        for (EdgeNode e:getInstance().edgeNodesMap.values()) {
            if (e.getId() == id)
                return e;
        }
        return null;
    }
    public EdgeNode getNearestNodeFromCoordinate(int x, int y){
        EdgeNode nearestNode = null;
        int minDistance = Utils.MAX_EDGES_ON_CITY+Utils.MAX_EDGES_ON_CITY;

        List<EdgeNode> list = new ArrayList<>();
        synchronized (getInstance().edgeNodesMap){
            list.addAll(getInstance().edgeNodesMap.values());
        }
        for (EdgeNode e:list) {
            if (e.getDistanceFrom(x,y)<minDistance){
                nearestNode = e;
                minDistance = e.getDistanceFrom(x,y);
            }
        }
        if(nearestNode != null){
            return nearestNode;
        }
        return null;
    }
    public synchronized void removeWithId(int id){
        getInstance().edgeNodesMap.remove(getInstance().getNodeWithId(id).getCoordinate());
    }

    /*
     * Un nodo edge per essere aggiunto non deve avere un id di un altro nodo nella città
     * */
    public synchronized boolean canAddNodeWithId(int id){
        for (EdgeNode e:getInstance().edgeNodesMap.values()) {
            if (e.getId() == id)
                return false;
        }
        return true;
    }
    /*
    * Un nodo edge per essere aggiunto non deve essere sovrapposto ad un altro nella stessa cella
    * e deve mantenere la distanza minima
    * */
    public synchronized boolean canAddNodeWithCoordinates(Pair<Integer,Integer> coordinates){
        return coordinates.getKey() < (Utils.MAX_EDGES_ON_CITY) &&
                coordinates.getValue() < Utils.MAX_EDGES_ON_CITY &&
                !edgeNodesMap.containsKey(coordinates) &&
                isValidNodeWithCoordinates(coordinates)
                ? true : false;

    }
    private synchronized boolean isValidNodeWithCoordinates(Pair<Integer,Integer> coordinate){
        for (Pair<Integer,Integer> c:getInstance().edgeNodesMap.keySet()) {
            if (Math.abs(c.getKey()-coordinate.getKey()) + Math.abs(c.getValue()-coordinate.getValue()) < MIN_DISTANCE_EDGES)
                return false;
        }
        return true;
    }


    public void add(GlobalDataStats data){
        synchronized (getInstance().globalStats){
            try {
                getInstance().globalStats.add(new GlobalStat(data.getGlobalAvg(),data.getTimestamp()));
                for (DataStats d: data.getLocalAvgStats()){
                    getInstance().globalStats.add(new GlobalStat(d.value,d.timestamp,d.edgeId));
                }
            } catch(Exception exc){
                System.err.print(exc.toString());
            }
        }
    }
    public GlobalStat getLatestGlobalStat() {
        ArrayList<GlobalStat> _list;
        synchronized (getInstance().globalStats) {
            _list = new ArrayList<>(getInstance().globalStats);
        }
        if(_list.size() > 0){
            for (int i=_list.size()-1;i>=0;i--){
                if (_list.get(i).isGlobal()){
                    return _list.get(i);
                }
            }
        }
        return null;
    }

    //se non trovato nodo edge ritorna null
    //-1 all
    public ArrayList<GlobalStat> getLastNStatsOfEdgeId(int edgeId, int n){
        ArrayList<GlobalStat> _list;
        ArrayList<GlobalStat> _last = new ArrayList<>();
        synchronized (getInstance().globalStats) {
            _list = new ArrayList<>(getInstance().globalStats);
        }
        boolean found = false;
        if(_list.size() > 0){
            for (int i=_list.size()-1;i>=0;i--){
                if(_last.size() == n){
                    break;
                }
                if (_list.get(i).edgeId == edgeId || (edgeId == -1)){
                    _last.add(_list.get(i));
                    found = true;
                }
            }
        }
        return found ? _last : null;
    }


}