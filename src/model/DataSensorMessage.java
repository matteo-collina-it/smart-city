package model;

import com.google.gson.Gson;
//Messaggio inviato dai sensori ai nodi
public class DataSensorMessage {
    private String id;
    private double value;
    private long timestamp;

    public DataSensorMessage(){}
    public DataSensorMessage(String id, double value,long timestamp){
        this.id = id;
        this.value = value;
        this.timestamp = timestamp;
    }
    public String toGsonString(){
        return new Gson().toJson(this);
    }
    public String getId(){
        return this.id;
    }
    public double getValue(){
        return this.value;
    }
    public double getTimestamp(){
        return this.timestamp;
    }

    @Override
    public String toString() {
        return "\nDataSensor:\tid) "+this.getId()+"  \tt) "+this.getTimestamp()+ "\tval)" + this.getValue();
    }
}
