package model.utils;

import javafx.util.Pair;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;

public final class Utils {
    public static final int MAX_EDGES_ON_CITY = 100;

    public static Pair<Integer,Integer> getRandomCoordinateCity(){
        return new Pair<>(getRandomPointInCity(),getRandomPointInCity());
    }
    public static int getRandomPointInCity(){
        return Utils.getRandomInt(0,MAX_EDGES_ON_CITY-1);
    }
    public static int getRandomInt(int min,int max){
        return new Random().nextInt(max - min + 1) + min;
    }
    public static final long getCurrentTimestamp(){
        return System.currentTimeMillis();
    }
    public static final String fromTimestampToDate(long timestamp){
        Date currentDate = new Date(timestamp);
        DateFormat df = new SimpleDateFormat("HH:mm:ss");
        return df.format(currentDate);
    }
    public static final String getCurrentHourMinSec(){
        return fromTimestampToDate(getCurrentTimestamp());
    }
}
