package model.utils.deserializer;

import model.DataStats;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.DeserializationContext;
import org.codehaus.jackson.map.JsonDeserializer;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.type.ArrayType;
import org.codehaus.jackson.map.type.CollectionType;
import org.codehaus.jackson.map.type.TypeFactory;
import org.codehaus.jackson.type.TypeReference;

import java.io.IOException;
import java.util.ArrayList;

public class ListDatastatsDeserializer extends JsonDeserializer<ArrayList<DataStats>> {

    @Override
    public ArrayList<DataStats> deserialize(JsonParser p, DeserializationContext ctxt){
        ObjectMapper mapper = new ObjectMapper();
        mapper.enable(DeserializationConfig.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY);

        try {
            ArrayList<DataStats> asList = mapper.readValue(
                    p, new TypeReference<ArrayList<DataStats>>() { });
            return asList;
        } catch (IOException e) {
            System.out.print("\n");
            e.printStackTrace();
        }
        return null;
    }

}
