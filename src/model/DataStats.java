package model;


import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="DataStats")
@XmlAccessorType(XmlAccessType.FIELD)
public class DataStats {
    public double value; //avg
    public long timestamp;
    public int edgeId;

    public DataStats(){}
    public DataStats(double value,long timestamp,int edgeId){
        this.value = value;
        this.timestamp = timestamp;
        this.edgeId = edgeId;
    }
    public DataStats(DataStats anotherDataStats){
        this.value = anotherDataStats.value;
        this.timestamp = anotherDataStats.timestamp;
        this.edgeId = anotherDataStats.edgeId;
    }
    public double getStat(){
        return this.value;
    }
}
