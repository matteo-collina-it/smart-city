package model;

import model.utils.Utils;
import model.utils.deserializer.ListDatastatsDeserializer;
import org.codehaus.jackson.map.annotate.JsonDeserialize;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.crypto.Data;
import java.util.ArrayList;

@XmlRootElement(name="GlobalStats")
@XmlAccessorType(XmlAccessType.FIELD)
public class GlobalDataStats {
    @JsonDeserialize(using = ListDatastatsDeserializer.class)
    private ArrayList<DataStats> listLocalAvgStats = new ArrayList<>();
    private double globalAvg;
    private long timestamp;

    public GlobalDataStats(){}
    public GlobalDataStats(ArrayList<DataStats> listLocalAvgStats,double globalAvg,long timestamp){
        this.listLocalAvgStats = new ArrayList<>();
        for (DataStats d:listLocalAvgStats) {
            this.listLocalAvgStats.add(d);
        }
        this.globalAvg = globalAvg;
        this.timestamp = timestamp;
    }
    public ArrayList<DataStats> getLocalAvgStats(){
        return this.listLocalAvgStats;
    }
    public double getGlobalAvg(){
        return this.globalAvg;
    }
    public long getTimestamp(){
        return this.timestamp;
    }

    @Override
    public String toString(){
        String localAvgs = "";

        for(int i=0;i<this.getLocalAvgStats().size();i++){
            localAvgs += ((this.getLocalAvgStats().get(i)).getStat() + (i==this.getLocalAvgStats().size()-1 ? "" : ", "));
        }

        return  "\n" + Utils.fromTimestampToDate(this.timestamp) +
                "\tGlobal AVG: " + Double.toString(this.getGlobalAvg()) +
                "\tTot local ("+this.getLocalAvgStats().size()+")  [" + localAvgs +  "]";
    }
}
