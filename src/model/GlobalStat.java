package model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="GlobalStat")
@XmlAccessorType(XmlAccessType.FIELD)
public class GlobalStat {
    public double value;
    public long timestamp;
    public int edgeId; //0 if global stat

    private static final int IS_GLOBAL = 0;

    public GlobalStat(){}
    public GlobalStat(double value, long timestamp){
        this.value = value;
        this.timestamp = timestamp;
        this.edgeId = IS_GLOBAL;
    }
    public GlobalStat(double value, long timestamp, int edgeId){
        this.value = value;
        this.timestamp = timestamp;
        this.edgeId = edgeId;
    }

    public double getValue(){
        return this.value;
    }
    public long getTimestamp(){
        return this.timestamp;
    }
    public boolean isGlobal(){
        return this.edgeId == IS_GLOBAL;
    }
    public boolean isLocal(){
        return !this.isGlobal();
    }
}
