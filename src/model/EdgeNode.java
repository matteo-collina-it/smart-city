package model;

import javafx.util.Pair;
import services.ServiceManager;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="EdgeNode")
@XmlAccessorType(XmlAccessType.FIELD)
public class EdgeNode {
    private int id;
    private int portListenFromEdges;
    private int portListenFromSensor;
    private String hostServerCloud;
    private int x;
    private int y;
    private String ip;
    private boolean iAmCoordinator = false;

    public static final int NO_VALUE = -1;
    private static final String NOT_SET = "-";

    public EdgeNode(){}
    public EdgeNode(int id, int portListenFromEdges, int portListenFromSensor, String hostServerCloud){
        this.id = id;
        this.portListenFromEdges = portListenFromEdges;
        this.portListenFromSensor = portListenFromSensor;
        this.hostServerCloud = hostServerCloud;
        this.x = NO_VALUE;
        this.y = NO_VALUE;
        this.ip = NOT_SET;
    }
    public EdgeNode(int id, String ip, int portListenFromEdges, int portListenFromSensor, int x, int y){
        this.id = id;
        this.ip = ip;
        this.portListenFromSensor = portListenFromSensor;
        this.portListenFromEdges = portListenFromEdges;
        this.x = x;
        this.y = y;
        this.hostServerCloud = NOT_SET;
    }
    //complete
    public EdgeNode(int id, int portListenFromEdges, int portListenFromSensor, String hostServerCloud, int x, int y, String ip){
        this.id = id;
        this.portListenFromEdges = portListenFromEdges;
        this.portListenFromSensor = portListenFromSensor;
        this.hostServerCloud = hostServerCloud;
        this.x = x;
        this.y = y;
        this.ip = ip;
    }


    /* GET */
    public int getId() {
        return this.id;
    }
    public int getPortListenFromEdges() {
        return this.portListenFromEdges;
    }
    public int getPortListenFromSensor() {
        return this.portListenFromSensor;
    }
    public String getHostServerCloud() {
        return this.hostServerCloud;
    }
    public int getX() {
        return this.x;
    }
    public int getY() {
        return this.y;
    }
    public String getIp() {
        return this.ip;
    }
    public String getHttpIp() {
        return "127.0.0.1";//ServiceManager.PROTOCOL + this.ip;
    }
    public Pair<Integer,Integer> getCoordinate() {
        return new Pair<>(this.x,this.y);
    }
    public int getDistanceFrom(EdgeNode edge) {
        return Math.abs(this.x - edge.x) + Math.abs(this.y - edge.y);
    }
    public int getDistanceFrom(int x, int y) {
        return Math.abs(this.x - x) + Math.abs(this.y - y);
    }
    public boolean amICoordinator() {
        return this.iAmCoordinator;
    }

    /* SET */
    public void setId(int id) {
        this.id = id;
    }
    public void setPortListenFromEdges(int portListenFromEdges) {
        this.portListenFromEdges = portListenFromEdges;
    }
    public void setPortListenFromSensor(int portListenFromSensor) {
        this.portListenFromSensor = portListenFromSensor;
    }
    public void setHostServerCloud(String hostServerCloud) {
        this.hostServerCloud = hostServerCloud;
    }
    public void setX(int x) {
        this.x = x;
    }
    public void setY() {
        this.y = y;
    }
    public void setIp() {
        this.ip = ip;
    }
    public void setiAmCoordinator() {
        this.iAmCoordinator = true;
    }

    @Override
    public String toString(){
       return  "\tID: " + this.getId() +
               "\tPort Listen From Edges: " + this.getPortListenFromEdges() +
               "\tPort Listen From Sensor: " + this.getPortListenFromSensor() +
               "\tPosition (" + this.getX() + "," + this.getY() + ")\n";
    }


    public static int getDistanceBetween(int x1, int y1, int x2, int y2) {
        return Math.abs(x1 - x2) + Math.abs(y1 - y2);
    }
}
