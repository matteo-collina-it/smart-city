package model;

import com.google.gson.Gson;

public class EdgeNodeMessage {
    public enum MessageType {
        NONE,NEW_NODE,ELECTION,OK,COORDINATOR,PING,STATS,GLOBAL_STATS,OK_ELECTION,STOP_ELECTION
    }
    private MessageType messageType;
    private EdgeNode edgeNode;
    private DataStats dataStats;
    private GlobalStat globalStat;

    public EdgeNodeMessage(){}
    public EdgeNodeMessage(MessageType messageType){
        this.messageType = messageType;
    }
    public EdgeNodeMessage(MessageType messageType, EdgeNode edgeNode){
        this.messageType = messageType;
        this.edgeNode = edgeNode;
    }
    public EdgeNodeMessage(MessageType messageType, EdgeNode edgeNode, DataStats dataStats){
        this.messageType = messageType;
        this.edgeNode = edgeNode;
        this.dataStats = dataStats;
    }
    public EdgeNodeMessage(MessageType messageType, EdgeNode edgeNode, GlobalStat globalStat){
        this.messageType = messageType;
        this.edgeNode = edgeNode;
        this.globalStat = globalStat;
    }
    public String toGsonString(){
        return new Gson().toJson(this);
    }

    public MessageType getMessageType(){
        return this.messageType;
    }
    public String valueOfMessageType(){
        switch (this.messageType){
            case NEW_NODE:
                return "NEW_NODE";
            case ELECTION:
                return "ELECTION";
            case OK:
                return "OK";
            case COORDINATOR:
                return "COORDINATOR";
            case STATS:
                return "STATS";
            case GLOBAL_STATS:
                return "GLOBAL_STATS";
            case OK_ELECTION:
                return "OK_ELECTION";
            case STOP_ELECTION:
                return "STOP_ELECTION";
            case PING:
                return "PING";
                default:
                    return "NONE";
        }
    }
    public EdgeNode getEdgeNode(){
        return this.edgeNode;
    }
    public DataStats getStats(){
        return this.dataStats;
    }
    public GlobalStat getGlobalStats(){
        return this.globalStat;
    }

    @Override
    public String toString() {
        String global = "\n" + (this.globalStat != null ? ("Global AVG: " + Double.toString(this.globalStat.getValue())) : "");
        return "\nEdgeNode Id: "+this.getEdgeNode().getId() +
                "\nMessage type: "+this.valueOfMessageType() +
                global;
    }
}
