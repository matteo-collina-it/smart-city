package model;

public class CounterMsgElection {
    private int counter = 0;
    public CounterMsgElection(){
        this.counter = 0;
    }
    public synchronized void reset(){
        this.counter = 0;
    }
    public synchronized void addOne(){
        this.counter += 1;
    }
    public synchronized int size(){
        return this.counter;
    }
}
