package structure;

import model.DataSensorMessage;

import java.util.LinkedList;
import java.util.Queue;

public class BufferLocalStats<T> {

    private Queue<T> queue = new LinkedList<T>();
    private int capacity;
    
    public BufferLocalStats(int capacity) {
        this.capacity = capacity;
    }

    public BufferLocalStats(BufferLocalStats<DataSensorMessage> buffer) {
        this.queue = new LinkedList(buffer.queue);
        this.capacity = buffer.capacity;
    }

    public synchronized void put(T element) throws InterruptedException {
        while(queue.size() == capacity) {
            //System.out.print("\n___ WAIT (queue.size() == capacity)");
            wait();
        }
        queue.add(element);
        //System.out.print("\n___ ADD "+element.toString()+" (size "+queue.size()+")");
    }
    public synchronized int size() {
        return queue.size();
    }
    public synchronized T take() throws InterruptedException {
        while(queue.isEmpty()) {
            //System.out.print("\n___ WAIT queue.isEmpty()");
            wait();
        }
        T item = queue.remove();
        if(queue.size() <= this.capacity/2){
            //System.out.print("\n___ NOTIFYALL queue.size() <= this.capacity/2");
            notifyAll();
        }
        return item;
    }
}
