package services;

import java.net.ServerSocket;
import java.util.Random;

public final class ServiceManager {
    public static String PROTOCOL =  "http://";
    public static String BASE_URL =  "localhost";
    public static int BASE_PORT =  1337;
    public static int MAX_PORT =  65535;
    public static int MIN_PORT =  1024;

    public static final class URL {
        public static final class EDGE {
            public static final String ADD = "add";
            public static final String LIST = "list";
            public static final String DELETE = "delete";
            public static final String NEAREST = "nearest";
            public static final String ADDSTATS = "addstats";
            public static final String RECENTSTATS = "recentstats";
            public static final String LASTLOCALSTATS = "lastlocalstats";
            public static final String STANDARDDEVIATION = "deviation";
        }
        public static final class ANALYST {
            public static final String STATUS_CITY = "statusCity";
        }
    }
    public static final class ENTITY_TYPE {
        public static String EDGE = "edge";
        public static String ANALYST = "analyst";
    }
    /*
     * Get socket
     * 1-65535 are available ports
     * 0 is reserved
     * 1-1023 are the privileged ones
     * */
    public static int getSocket(){
        try {
            ServerSocket serverSocket =  new ServerSocket(0);
            int port = serverSocket.getLocalPort();
            serverSocket.close();
            //System.out.print("\nChecking Port " + port + "... -> is available");
            return port;
        }catch (Exception e){
            System.err.println(e);
            return new Random().nextInt(MAX_PORT - MIN_PORT + 1) + MIN_PORT;
        }
    }
}
