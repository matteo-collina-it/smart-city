package services.edge;

import javafx.util.Pair;
import model.*;
import model.singleton.EdgeNodeSingleton;
import services.ServiceManager;

import javax.ws.rs.*;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

@Path("analyst")
public class AnalystService {

    //@Override
    @Path(ServiceManager.URL.ANALYST.STATUS_CITY)
    @GET
    @Produces({"application/xml", "application/json"})
    public Response getStatusCity() {
        GenericEntity<List<EdgeNode>> entity =
                new GenericEntity<List<EdgeNode>>(EdgeNodeSingleton.getInstance().getEdgeNodesList()) {};
        return Response.ok().entity(entity).build();
    }
}
