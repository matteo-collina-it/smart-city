package services.edge;

import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.GenericType;
import com.sun.jersey.api.client.WebResource;
import model.*;
import model.utils.Utils;
import services.ServiceManager;

import com.sun.jersey.api.client.Client;


import com.sun.jersey.api.client.WebResource.Builder;

import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;


public class CityClient {
    private static Client client = Client.create();
    private static Builder builder;
    private static final int MAX_MISS = 10;

    private enum EntityType {
        EDGE,
        ANALYST
    }

    /* ----- EDGE ----- */
    public List<EdgeNode> addNode(EdgeNode node) {
        builder = client
                .resource(getUrlForEdgeService(ServiceManager.URL.EDGE.ADD))
                .type(MediaType.APPLICATION_JSON_TYPE);
        int miss = -1;
        ClientResponse response;
        do {
            response = builder.post(ClientResponse.class, node);
            miss+=1;
            //System.out.print("\n... Provo ad inserire il nodo ...");
            if(miss==MAX_MISS-1){
                System.err.print("\nNon è stato possibile aggiungere alla città il nodo " +
                        node.toString());
                return new ArrayList();
            }
        }while(response.getStatus() != Response.Status.OK.getStatusCode() && miss < MAX_MISS);


        System.out.print("\n"+ Utils.getCurrentHourMinSec()+"\tInserito il nodo:" + node.toString() );

        List<EdgeNode> list = response.getEntity(new GenericType<List<EdgeNode>>(){});
        return list;
    }

    public List<EdgeNode> getListNodes() {
        builder = client
                .resource(getUrlForEdgeService(ServiceManager.URL.EDGE.LIST))
                .type(MediaType.APPLICATION_JSON_TYPE);

        try{
            ClientResponse response = builder.get(ClientResponse.class);
            List<EdgeNode> list = response.getEntity(new GenericType<List<EdgeNode>>(){});
            return list;
        }catch (Exception ex){
            System.err.print("\ngetListNodes ERROR");
            ex.printStackTrace();
            return null;
        }

    }

    public boolean removeNode(EdgeNode node) {
        builder = client
                .resource(getUrlForEdgeService(Integer.toString(node.getId())))
                .type(MediaType.APPLICATION_JSON_TYPE);

        ClientResponse response;
        int miss = 0;
        do {
            response = builder.delete(ClientResponse.class);
            System.out.print("\n... Provo a rimuovere il nodo ...");
            if(miss==MAX_MISS-1){
                System.err.print("\nNon è stato possibile rimuovere il nodo " + node.toString());
                return false;
            }
        }while(response.getStatus() != Response.Status.OK.getStatusCode() && miss < MAX_MISS);

        System.out.print("\nRimosso il nodo" + node.toString() );
        return true;
    }
    public EdgeNode getNearestNode(int x, int y) {
        builder = client
                .resource(getUrlForEdgeService(ServiceManager.URL.EDGE.NEAREST) + "/" + x + "/" + y)
                .type(MediaType.APPLICATION_JSON_TYPE);
        ClientResponse response = builder
                .get(ClientResponse.class);
        if(response.getStatus() != 200){
            System.err.println("Unable to connect to the server");
            return null;
        }
        return response.getEntity(EdgeNode.class);
    }


    public boolean sendGlobalStat(GlobalDataStats globalDataStats) {
        builder = client
                .resource(getUrlForEdgeService(ServiceManager.URL.EDGE.ADDSTATS))
                .type(MediaType.APPLICATION_JSON_TYPE);
        try{
            ClientResponse response = builder.post(ClientResponse.class, globalDataStats);
            return true;
        }catch(Exception e){
            System.out.print("\n");
            e.printStackTrace();
            return false;
        }
    }

    public GlobalStat getLatestGlobalStat() {
        builder = client
                .resource(getUrlForEdgeService(ServiceManager.URL.EDGE.RECENTSTATS))
                .type(MediaType.APPLICATION_JSON_TYPE);

        try{
            ClientResponse response = builder.get(ClientResponse.class);
            return response.getEntity(GlobalStat.class);
        }catch(Exception e){
            System.out.print("\n");
            e.printStackTrace();
            return null;
        }
    }


    /* ----- ANALYST ----- */
    public List<EdgeNode> getStatusCity() {
        builder = client
                .resource(getUrlForAnalyst(ServiceManager.URL.ANALYST.STATUS_CITY))
                .type(MediaType.APPLICATION_JSON_TYPE);
        try{
            ClientResponse response = builder.get(ClientResponse.class);
            List<EdgeNode> list = response.getEntity(new GenericType<List<EdgeNode>>(){});
            return list;
        }catch (Exception ex){
            System.err.print("\ngetListNodes ERROR");
            ex.printStackTrace();
            return null;
        }
    }

    public List<GlobalStat> getLastNStatsOfEdgeId(int n,int edgeId) {
        builder = client
                .resource(getUrlForEdgeService(ServiceManager.URL.EDGE.LASTLOCALSTATS) + "/" + edgeId + "/" + n)
                .type(MediaType.APPLICATION_JSON_TYPE);
        ClientResponse response = builder
                .get(ClientResponse.class);

        if(response.getStatus() == 200){
            try{
                List<GlobalStat> list = response.getEntity(new GenericType<List<GlobalStat>>(){});
                return list;
            }catch(Exception e){
                System.out.print("\n");
                e.printStackTrace();
                return null;
            }
        }else{
            return null;
        }
    }

    public List<GlobalStat> getLastNStatsOfCity(int n) {
        return getLastNStatsOfEdgeId(n,-1);
    }


    public StandardDeviation getDeviationOfNStatsOfEdgeId(int n, int edgeId) {
        builder = client
                .resource(getUrlForEdgeService(ServiceManager.URL.EDGE.STANDARDDEVIATION) + "/" + edgeId + "/" + n)
                .type(MediaType.APPLICATION_JSON_TYPE);
        ClientResponse response = builder
                .get(ClientResponse.class);

        if(response.getStatus() == 200){
            try{
                StandardDeviation std = response.getEntity(StandardDeviation.class);
                return std;
            }catch(Exception e){
                System.out.print("\n");
                e.printStackTrace();
                return null;
            }
        }else{
            return null;
        }
    }

    public StandardDeviation getDeviationOfNStatsOfCity(int n) {
        return getDeviationOfNStatsOfEdgeId(n,-1);
    }




    //Request for client edge
    private static String getUrlForEdgeService(String service){
        return getUrlForService(service,EntityType.EDGE);
    }
    //Request for client analyst
    private static String getUrlForAnalyst(String service){
        return getUrlForService(service,EntityType.ANALYST);
    }

    private static String getUrlPartForEntityType(EntityType type){
        switch (type) {
            case EDGE: {
                return ServiceManager.ENTITY_TYPE.EDGE;
            }
            case ANALYST: {
                return ServiceManager.ENTITY_TYPE.ANALYST;
            }
            default: {
                return ServiceManager.ENTITY_TYPE.EDGE;
            }
        }
    }
    private static String getUrlForService(String service, EntityType type){
        return ServiceManager.PROTOCOL + ServiceManager.BASE_URL + ":" + ServiceManager.BASE_PORT + "/" + getUrlPartForEntityType(type) + '/' + service;
    }
}