package services.edge;

import model.AppException;
import model.EdgeNode;
import model.GlobalDataStats;
import services.ServiceManager;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;

public interface EdgeNodeInterface extends ExceptionMapper<AppException> {
    /*
     * @return lista di nodi edge nella città
     * */
    @Path("list")
    @GET
    @Produces({"application/xml", "application/json"})
    public Response getEdgeNodesList();

    /*
     * @param nodo edge
     * @return lista dei nodi edge nella città / Conflict
     * */
    @Path("add")
    @POST
    @Consumes({"application/xml", "application/json"})
    @Produces({"application/xml", "application/json"})
    public Response add(EdgeNode e);

    /*
     * @param id del nodo edge da cancellare
     * @return Response Ok / Not Found
     * */
    @Path("{id}")
    @DELETE
    @Consumes({"application/json"})
    @Produces("application/json")
    public Response remove(@PathParam("id") int id);



    /*
     * @param x longitudine del sensore
     * @param y latitudine del sensore
     * @return Response con il nodo edge più vicino (se presente) / Not Found
     * */
    @Path("nearest/{x}/{y}")
    @GET
    @Consumes({"application/json"})
    @Produces({"application/json"})
    public Response getNearestEdgeNodeToSensorPosition(@PathParam("x") int x, @PathParam("y") int y);


    /*
     * @param globalDataStats: stats avg locali e stat globale
     * @return Response T/F
     * */
    @Path(ServiceManager.URL.EDGE.ADDSTATS)
    @POST
    @Consumes({"application/json"})
    @Produces({"application/json"})
    public Response addGlobalStats(GlobalDataStats globalDataStats);


    /*
     * @return Response Latest Global Stat
     * */
    @Path(ServiceManager.URL.EDGE.RECENTSTATS)
    @GET
    @Produces({"application/json"})
    public Response getLatestGlobalStat();


    @Path("lastlocalstats/{id}/{n}")
    @GET
    @Consumes({"application/xml", "application/json"})
    @Produces({"application/xml", "application/json"})
    public Response getLastNStatsOfEdgeId(@PathParam("id") int edgeId, @PathParam("n") int n);


    /*
    * Reference to deviation standard: https://www.okpedia.it/deviazione-standard-scarto-quadratico-medio
    * */
    @Path("deviation/{id}/{n}")
    @GET
    @Consumes({"application/xml", "application/json"})
    @Produces({"application/xml", "application/json"})
    public Response getDeviationOfLastNStatsOfEdgeId(@PathParam("id") int edgeId, @PathParam("n") int n);
}
