package services.edge;


import javafx.util.Pair;
import model.*;
import model.singleton.EdgeNodeSingleton;
import services.ServiceManager;

import javax.ws.rs.*;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.ws.Service;
import java.util.ArrayList;
import java.util.List;


@Path("edge")
public class EdgeNodeService implements EdgeNodeInterface {

    @Override
    @Path(ServiceManager.URL.EDGE.LIST)
    @GET
    @Produces({"application/xml", "application/json"})
    public Response getEdgeNodesList() {
        GenericEntity<List<EdgeNode>> entity =
                new GenericEntity<List<EdgeNode>>(EdgeNodeSingleton.getInstance().getEdgeNodesList()) {};
        return Response.ok().entity(entity).build();
    }

    @Override
    @Path(ServiceManager.URL.EDGE.ADD)
    @POST
    @Consumes({"application/xml", "application/json"})
    @Produces({"application/xml", "application/json"})
    public Response add(EdgeNode e){
        Pair<Integer, Integer> coordinate = new Pair<>(e.getX(), e.getY());
        String msgError = "";
        if (!EdgeNodeSingleton.getInstance().canAddNodeWithId(e.getId())) {
            msgError = "Il nodo edge con id " + String.valueOf(e.getId()) + " è già presente nella città";
        } else if (!EdgeNodeSingleton.getInstance().canAddNodeWithCoordinates(coordinate)) {
            msgError = "Le coordinate specificate per il nodo edge " +
                    "(" + coordinate.getKey() + "," + coordinate.getValue() + ") non sono valide";
        } else if(e.getId() == EdgeNode.NO_VALUE){
            msgError = "Il nodo edge non può avere un id uguale al valore " + String.valueOf(EdgeNode.NO_VALUE);
        }else {
            EdgeNodeSingleton.getInstance().add(e);
            GenericEntity<List<EdgeNode>> entity =
                    new GenericEntity<List<EdgeNode>>(EdgeNodeSingleton.getInstance().getEdgeNodesList()) {};
            return Response.ok().entity(entity).build();
        }
        System.err.print("\n!!" + msgError + " !!");
        return toResponse(new AppException(
                Response.Status.CONFLICT.getStatusCode(),
                Response.Status.CONFLICT.getStatusCode(),
                msgError,
                msgError,
                "add/"+e.toString()));
    }


    @Override
    @Path("{id}")
    @DELETE
    @Consumes({"application/json"})
    @Produces("application/json")
    public Response remove(@PathParam("id") int id){
        try {
            EdgeNodeSingleton.getInstance().removeWithId(id);
            return Response.ok().build();
        } catch (Exception ex){
            return toResponse(new AppException(
                    Response.Status.NOT_FOUND.getStatusCode(),
                    Response.Status.NOT_FOUND.getStatusCode(),
                    "Non è stato possibile rimuovere il nodo edge indicato",
                    "Non è stato possibile rimuovere il nodo edge indicato con id "+String.valueOf(id)+" ",
                    "delete/"+String.valueOf(id)));
        }
    }


    @Override
    @Path("nearest/{x}/{y}")
    @GET
    @Consumes({"application/json"})
    @Produces({"application/json"})
    public Response getNearestEdgeNodeToSensorPosition(@PathParam("x") int x, @PathParam("y") int y) {
        EdgeNode edgeNode = EdgeNodeSingleton.getInstance().getNearestNodeFromCoordinate(x,y);
        if (edgeNode != null) {
            return Response.ok(edgeNode).build();
        } else{
            return toResponse(new AppException(
                    Response.Status.NOT_FOUND.getStatusCode(),
                    Response.Status.NOT_FOUND.getStatusCode(),
                    "Non è stato trovato alcun nodo vicino alle coordinate specificate",
                    "Non è stato trovato alcun nodo vicino alle coordinate specificate ("+
                            String.valueOf(x) + " , " +
                            String.valueOf(y) + ")",
                    "nearest/"+String.valueOf(x)+String.valueOf(y)));
        }

    }

    @Override
    @Path(ServiceManager.URL.EDGE.ADDSTATS)
    @POST
    @Consumes({"application/json"})
    @Produces({"application/json"})
    public Response addGlobalStats(GlobalDataStats globalDataStats){
        try {
            EdgeNodeSingleton.getInstance().add(globalDataStats);
            System.out.print(globalDataStats.toString());
            return Response.ok().build();
        } catch (Exception ex){
            return toResponse(new AppException(
                    Response.Status.NOT_FOUND.getStatusCode(),
                    Response.Status.NOT_FOUND.getStatusCode(),
                    "Non è stato possibile inviare la statistica globale",
                    "Non è stato possibile inviare la statistica globale",
                    "addstats/"));
        }
    }

    @Override
    @Path(ServiceManager.URL.EDGE.RECENTSTATS)
    @GET
    @Produces({"application/json"})
    public Response getLatestGlobalStat() {
        GlobalStat g = EdgeNodeSingleton.getInstance().getLatestGlobalStat();
        if(g != null){
            return Response.ok(g).build();
        }else{
            System.out.print("\n-> Invalid last global stat");
            return toResponse(new AppException(
                    Response.Status.NOT_FOUND.getStatusCode(),
                    Response.Status.NOT_FOUND.getStatusCode(),
                    "Non sono presenti statistiche globali",
                    "Non sono presenti statistiche globali",
                    "recentstats/"));
        }
    }



    @Override
    @Path("lastlocalstats/{id}/{n}")
    @GET
    @Consumes({"application/xml", "application/json"})
    @Produces({"application/xml", "application/json"})
    public Response getLastNStatsOfEdgeId(@PathParam("id") int edgeId, @PathParam("n") int n) {
        GenericEntity<List<GlobalStat>> entity =
                new GenericEntity<List<GlobalStat>>(EdgeNodeSingleton.getInstance().getLastNStatsOfEdgeId(edgeId,n)) {};

        if(entity != null){
            return Response.ok(entity).build();
        }else{
            return toResponse(new AppException(
                    Response.Status.NOT_FOUND.getStatusCode(),
                    Response.Status.NOT_FOUND.getStatusCode(),
                    edgeId == -1 ? "Non sono presenti statistiche" : "Il nodo indicato non ha alcuna statistica registrata",
                    edgeId == -1 ? "Non sono presenti statistiche" : "Il nodo indicato non ha alcuna statistica registrata",
                    ServiceManager.URL.EDGE.LASTLOCALSTATS));
        }
    }


    @Override
    @Path("deviation/{id}/{n}")
    @GET
    @Consumes({"application/xml", "application/json"})
    @Produces({"application/xml", "application/json"})
    public Response getDeviationOfLastNStatsOfEdgeId(@PathParam("id") int edgeId, @PathParam("n") int n) {
        ArrayList<GlobalStat> list = EdgeNodeSingleton.getInstance().getLastNStatsOfEdgeId(edgeId,n);
        if(list != null && list.size() > 0){

            //Calcolo AVG
            double avg = 0;
            for (GlobalStat g:list){
                avg += g.value;
            }
            avg /= list.size();

            //Calcolo sommatoria al num
            double sommatoria = 0;
            for (GlobalStat g:list){
                sommatoria += Math.pow((g.value - avg),2);
            }

            //Calcolo deviazione std
            double standardDeviation = Math.sqrt(sommatoria/list.size());

            StandardDeviation entity = new StandardDeviation(standardDeviation,avg);
            return Response.ok(entity).build();
        }else{
            return toResponse(new AppException(
                    Response.Status.NOT_FOUND.getStatusCode(),
                    Response.Status.NOT_FOUND.getStatusCode(),
                    edgeId == -1 ? "Non sono presenti statistiche" : "Il nodo indicato non ha alcuna statistica registrata",
                    edgeId == -1 ? "Non sono presenti statistiche" : "Il nodo indicato non ha alcuna statistica registrata",
                    ServiceManager.URL.EDGE.LASTLOCALSTATS));
        }
    }

    @Override
    public Response toResponse(AppException ex) {
        return Response
                .status(Response.Status.CONFLICT.getStatusCode())
                .entity(new ErrorMessage(ex))
                .type(MediaType.APPLICATION_JSON)
                        .build();
    }
}