package simulator;

public interface SensorStream {

    void sendMeasurement(Measurement m);

}
